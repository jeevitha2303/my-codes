package com.tvm.Spring.Employee;

public class EmployeePojo {
	int Empid;
	String Empname;
	int age;
	int Salary;
	public String getEmpname() {
		return Empname;
	}
	public void setEmpname(String empname) {
		Empname = empname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return Salary;
	}
	public void setSalary(int salary) {
		Salary = salary;
	}
	public EmployeePojo(int empid, String empname, int age, int salary) {
		super();
		Empid = empid;
		Empname = empname;
		this.age = age;
		Salary = salary;
	}
	@Override
	public String toString() {
		return "EmployeePojo [Empid=" + Empid + ", Empname=" + Empname + ", age=" + age + ", Salary=" + Salary + "]";
	}
	public EmployeePojo() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Empid;
		result = prime * result + ((Empname == null) ? 0 : Empname.hashCode());
		result = prime * result + Salary;
		result = prime * result + age;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeePojo other = (EmployeePojo) obj;
		if (Empid != other.Empid)
			return false;
		if (Empname == null) {
			if (other.Empname != null)
				return false;
		} else if (!Empname.equals(other.Empname))
			return false;
		if (Salary != other.Salary)
			return false;
		if (age != other.age)
			return false;
		return true;
	}
	
	
	

}
